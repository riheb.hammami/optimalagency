<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Property;
use App\Repository\PropertyRepository;

class PropertyController extends AbstractController
{ 
    
    /**
     * @Route("/properties", name="properties")
     */
    public function index()
    {
        // $property=new Property();
        // $property->setTitle('Mon premier bien')
        // ->setDescription('une petite description')
        // ->setPrice(2000000)
        // ->setRooms(4)
        // ->setSurface(4000)
        // ->setBedrooms(3)
        // ->setFloor(4)
        // ->setHeat(1)
        // ->setCity('Carriers sous Poissy')
        // ->setAdress('Rue de la senette')
        // ->setPostalCode(78955);
        // $em=$this->getDoctrine()->getManager();
        // $em->persist($property);
        // $em->flush();   
        $repository=$this->getDoctrine()->getRepository(Property::class)->find(1);
        dump($repository);  
        return $this->render('property/index.html.twig', [
            'current_menu' => 'proporties',
        ]);
    }


       /**
     * @Route("/properties/{slug}-{id}", name="property.show",requirements={"slug"="[a-z0-9\-]*"})
     */
    public function show(Property $property,string $slug)
    {

        if($property->getSlug()!==$slug){
            return $this->redirectToRoute('property.show',
            ['id'=>$property->getId(),
            'slug'=>$property->getSlug()],301);
        }
        return $this->render('property/show.html.twig', [
            'property'=>$property,
            'current_menu' => 'proporties'
        ]);
    }
}
