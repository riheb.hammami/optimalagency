<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Property;
use App\Form\PropertyType;
use Symfony\Component\HttpFoundation\Request;


class PropertyController extends AbstractController
{
    /**
     * @Route("/admin/properties", name="admin.property.index")
     */
    public function index()
    {
        $properties=$this->getDoctrine()->getRepository(Property::class)->findAll();
        return $this->render('admin/property/index.html.twig', compact('properties'));
    }

    
     /**
     * @Route("/admin/properties/add", name="admin.property.add")
     */
    public function add( Request $request)
    {
        $property=new Property();
        $form = $this->createForm(PropertyType::class, $property);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($property);
            $em->flush();
            $this->addFlash('success','Ajout effectué avec succés');
            return $this->redirectToRoute('admin.property.index');
        }
        
        return $this->render('admin/property/add.html.twig', ['property' =>$property,
        'form'=>$form->createView()]);
    }



     /**
     * @Route("/admin/properties/{id}", name="admin.property.edit", methods="GET|POST")
     */
    public function edit(Property $property, Request $request)
    {
        $form = $this->createForm(PropertyType::class, $property);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            // $em->persist($property);
            $em->flush();
            $this->addFlash('success','Modification effectuée avec succés');
            return $this->redirectToRoute('admin.property.index');

        }
        
        return $this->render('admin/property/edit.html.twig', ['property' =>$property,
        'form'=>$form->createView()]);
    }
    
    /**
     * @Route("/admin/properties/{id}", name="admin.property.delete",methods="DELETE")
     */
    public function delete(Property $property, Request $request)
    {

       $submittedToken = $request->request->get('token');
        // 'delete-item' is the same value used in the template to generate the token
        if ($this->isCsrfTokenValid('delete-item', $submittedToken)) {
             $em = $this->getDoctrine()->getManager();
             $em->remove($property);
             $em->flush();
             $this->addFlash('success','Suppression effectuée avec succés');

        }

        return $this->redirectToRoute('admin.property.index');

        }

}
